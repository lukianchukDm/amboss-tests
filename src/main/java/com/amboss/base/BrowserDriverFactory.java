package com.amboss.base;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.remote.CapabilityType;

public class BrowserDriverFactory {

    private ThreadLocal<WebDriver> driver = new ThreadLocal<WebDriver>();
    private String browser;

    public BrowserDriverFactory(String browser) {
        this.browser = browser.toLowerCase();
    }

    public WebDriver createDriver() {
        System.out.println("Create driver: " + browser);
        switch (browser) {
            case "chrome":
                createChromedriver();
                break;

            case "firefox":
                createFirefoxdriver();
                break;

            default:
                createChromedriver();
                break;
        }

        return driver.get();
    }

    private void createChromedriver() {
        ChromeOptions chromeOptions = new ChromeOptions();

        chromeOptions.addArguments("--incognito");
        System.setProperty("webdriver.chrome.driver", "src/main/resources/chromedriver");

        driver.set(new ChromeDriver(chromeOptions));
    }

    private void createFirefoxdriver() {
        FirefoxOptions firefoxOptions = new FirefoxOptions();

        firefoxOptions.addArguments("-private");
        firefoxOptions.addPreference("extensions.allowPrivateBrowsingByDefault", true);

        System.setProperty("webdriver.gecko.driver", "src/main/resources/geckodriver");

        driver.set(new FirefoxDriver(firefoxOptions));
    }
}

