package com.amboss.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;

public class LoginPage extends BasePageObject {

    private String pageURL = "https://qa-assignment.us.next.medicuja.de/us/";

    private String email = "qa+assignment03@medicuja.com";
    private String password = "Gottafindthemall!";

    private By chromeAdvancedButtonLocator = By.cssSelector("button#details-button");
    private By chromeAcceptButtonLocator = By.cssSelector("#proceed-link");
    private By firefoxAdvancedButtonLocator = By.cssSelector("#advancedButton");
    private By firefoxAcceptButtonLocator = By.cssSelector("#exceptionDialogButton");

    private By emailInputLocator = By.cssSelector("#signin_username");
    private By passwordInputLocator = By.cssSelector("#signin_password");
    private By logInButtonLocator = By.cssSelector("[type='submit']");

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Workaround due to certificate failure
     */
    private LoginPage bypassSecurityMessage() {
        for (int i = 0; i < 2; i++) {
            String browserName = getBrowserName();

            if (browserName.equals("chrome")) {
                if (getCurrentPageTitle().equals("Privacy error")) {
                    click(chromeAdvancedButtonLocator).click(chromeAcceptButtonLocator);
                }
            } else if (browserName.equals("firefox")) {
                if (getCurrentPageTitle().equals("Warning: Potential Security Risk Ahead")) {
                    click(firefoxAdvancedButtonLocator).click(firefoxAcceptButtonLocator);
                }
            } else System.out.println("Browser '" + browserName + "' is not recognised");
        }

        return this;
    }

    public LandingPage login() {
        openUrl(pageURL);
        bypassSecurityMessage()
                .fill(email, emailInputLocator)
                .fill(password, passwordInputLocator)
                .click(logInButtonLocator)
                .openUrlInNewTab(pageURL);

        return new LandingPage(driver);
    }
}
