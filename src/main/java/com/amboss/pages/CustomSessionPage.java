package com.amboss.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

public class CustomSessionPage extends BasePageObject {

    private By sliderLocator = By.xpath("//*[contains(@class, 'handleStyle')]");
    private By slideLocator = By.xpath("//*[contains(@class, 'railStyle')]");
    private By questionsNumberInputLocator = By.cssSelector("[data-e2e-test-id='inputQuestionNumber'] input");
    private By editTitlePencilLocator = By.cssSelector("[data-e2e-test-id='customSessionTitle'] svg");
    private By editTitleInputLocator = By.cssSelector("[data-e2e-test-id='modalInput']");
    private By startButtonLocator = By.cssSelector("[data-e2e-test-id='buttonCreateSession'] button");

    public CustomSessionPage(WebDriver driver) {
        super(driver);
    }

    /**
     * Open a filter modal with given name
     */
    public FilterModal openFilterModal(String modalName) {
        By locator = By.xpath("//*[text()='" + modalName + "']");

        click(locator);

        return new FilterModal(driver, modalName);
    }

    /**
     * Move slider to 0
     */
    public int setQuestionsSliderToZero() {
        scrollTo(questionsNumberInputLocator);

        WebElement slider = find(sliderLocator);
        int xOffset = -getStartPositionInPixels() - 12; //width of a handle is 24px

        Actions action = new Actions(driver);
        action.dragAndDropBy(slider, xOffset, 0).build().perform();

        return getQuestionsNumber();
    }

    /**
     * Move slider to mid point
     */
    public int setQuestionsSliderToMiddle() {
        scrollTo(questionsNumberInputLocator);

        WebElement slider = find(sliderLocator);
        int middleInPixels = getSlideWidth() / 2;
        int xOffset = -getStartPositionInPixels() + middleInPixels - 12; //width of a handle is 24px

        Actions action = new Actions(driver);
        action.dragAndDropBy(slider, xOffset, 0).build().perform();

        return getQuestionsNumber();
    }

    /**
     * Move slider to max value
     */
    public int setQuestionsSliderToMaxValue() {
        scrollTo(questionsNumberInputLocator);

        WebElement slider = find(sliderLocator);
        int widthInPixels = getSlideWidth();
        System.out.println(widthInPixels);
        int xOffset = widthInPixels - getStartPositionInPixels() - 12; //width of a handle is 24px

        Actions action = new Actions(driver);
        action.dragAndDropBy(slider, xOffset, 0).build().perform();

        return getQuestionsNumber();
    }

    /**
     * Get value from `Questions number` input
     */
    private int getQuestionsNumber() {
        String text = getAttribute("value", questionsNumberInputLocator);
        return Integer.parseInt(text);
    }

    /**
     * Get current position of a slider in pixels
     */
    private int getStartPositionInPixels() {
        int width = getSlideWidth() + 1;
        int totalSteps = Integer.parseInt(getAttribute("aria-valuemax", sliderLocator));
        int currentPosition = getQuestionsNumber();

        double step = (double) width / totalSteps;

        return (int) (step * currentPosition);
    }

    /**
     * Get total width of a slide in pixels
     */
    private int getSlideWidth() {
        return find(slideLocator).getSize().getWidth();
    }

    public String editTitle(String title) {
        click(editTitlePencilLocator)
                .fill(title, editTitleInputLocator);

        return getAttribute("value", editTitleInputLocator);
    }

    public void startSession() {
        click(startButtonLocator);
    }

    public boolean isStartButtonEnabled() {
//        String attribute = find(startButtonLocator).getAttribute("class");
//        System.out.println(attribute);
//        return attribute.contains("-disabled-");
        return find(startButtonLocator).isEnabled();
    }

}
