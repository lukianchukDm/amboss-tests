package com.amboss.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LandingPage extends BasePageObject {

    private By qBankLocator = By.cssSelector("[data-e2e-test-id='L0-Qbank']");
    private By customSessionLocator = By.cssSelector("[data-e2e-test-id='L0-secondlevel-Custom session']");
    private By targetPageTitleLocator = By.xpath("//*[text()='New Custom Session']");

    public LandingPage(WebDriver driver) {
        super(driver);
    }

    public CustomSessionPage openCustomSession() {
        click(qBankLocator).click(customSessionLocator);

        //workaround due to rendering failure
        try {
            waitForVisibilityOf(targetPageTitleLocator);
        } catch (Exception e) {
            refreshPage();
        }

        pause(1000);

        return new CustomSessionPage(driver);
    }
}
