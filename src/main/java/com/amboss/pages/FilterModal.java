package com.amboss.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class FilterModal extends BasePageObject {

    private String modalName;

    public FilterModal(WebDriver driver, String modalName) {
        super(driver);

        this.modalName = modalName;
    }

    public boolean isOpened() {
        String modalLocator = "//*[@data-e2e-test-id='modalContainer']";
        By locator = By.xpath(modalLocator + " //*[text()='" + modalName + "']");

        return find(locator).isDisplayed();
    }

    public Category getCategory(String categoryName) {
        return new Category(driver, categoryName);
    }

    public Category getTitleCategory() {
        return new Category(driver, "title");
    }
}
