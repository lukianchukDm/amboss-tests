package com.amboss.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Category extends BasePageObject {

    private String categoryName;
    private By locator;
    private String modalLocator = "//*[@data-e2e-test-id='modalContainer']";

    public Category(WebDriver driver, String categoryName) {
        super(driver);

        this.categoryName = categoryName;
        this.locator = buildXpathForCheckbox(categoryName);
    }

    public Category unfold() {
        By locator = buildXpathForCategory(categoryName);
        click(locator);

        return this;
    }

    public Category select() {
        By locator = buildXpathForCheckbox(categoryName);
        click(locator);

        return this;
    }

    public boolean isNeutralChecked() {
        String status = getAttribute("data-e2e-test-id", locator);

        return status.equals("checkbox-neutral_checked");
    }

    public boolean isUnchecked() {
        String status = getAttribute("data-e2e-test-id", locator);

        return status.equals("checkbox-active_unchecked");
    }

    public boolean isMinus() {
        String status = getAttribute("data-e2e-test-id", locator);

        return status.equals("checkbox-active_indeterminate");
    }

    public boolean isChecked() {
        String status = getAttribute("data-e2e-test-id", locator);

        return status.equals("checkbox-active_checked");
    }

    private By buildXpathForCategory(String text) {
        return By.xpath(modalLocator + " //*[text()='" + text + "']");
    }

    private By buildXpathForCheckbox(String text) {
        if (text.equals("title"))
            return By.xpath("//*[@data-e2e-test-id='modalContainer'] //*[contains(@class, 'header')] /div /div /div");
        else
            return By.xpath(modalLocator + " //*[@data-e2e-test-id='checkbox-" + text + "'] /div /div");
    }
}
