package com.amboss;

import com.amboss.base.TestUtilities;
import com.amboss.pages.CustomSessionPage;
import com.amboss.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class StartSessionTests extends TestUtilities {

    @Test
    public void startSessionWithCustomTitle() {
        LoginPage loginPage = new LoginPage(driver);
        String newTitle = "Test Title";

        CustomSessionPage page = loginPage.login()
                .openCustomSession();

        String actualNewTitle = page.editTitle(newTitle);
        Assert.assertEquals(actualNewTitle, newTitle);

        page.startSession();

        String pageTitle = page.getCurrentPageTitle();
        Assert.assertEquals(pageTitle, newTitle + " - AMBOSS");
        //This assertion is abstract, I don't actually know what to expect from this new page,
        // this is just something that supposed to fail
    }

    @Test
    public void settingQuestionCountToZeroDisablesStartButton() {
        SoftAssert softAssert = new SoftAssert();

        LoginPage loginPage = new LoginPage(driver);
        String newTitle = "Test Title";

        CustomSessionPage page = loginPage.login()
                .openCustomSession();

        softAssert.assertTrue(page.isStartButtonEnabled());

        page.setQuestionsSliderToZero();

        softAssert.assertFalse(page.isStartButtonEnabled());

        softAssert.assertAll();
    }
}
