package com.amboss;

import com.amboss.base.TestUtilities;
import com.amboss.pages.CustomSessionPage;
import com.amboss.pages.LoginPage;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class QuestionCountTests extends TestUtilities {

    @Test
    public void sliderValueAffectsInputValue() {
        SoftAssert softAssert = new SoftAssert();
        LoginPage loginPage = new LoginPage(driver);

        CustomSessionPage page = loginPage.login().openCustomSession();

        int questions = page.setQuestionsSliderToZero();
        softAssert.assertEquals(questions, 0);

        questions = page.setQuestionsSliderToMaxValue();
        softAssert.assertEquals(questions, 2539);

        questions = page.setQuestionsSliderToMiddle();
        softAssert.assertEquals(questions, 1270);
        softAssert.assertAll();
    }
}
