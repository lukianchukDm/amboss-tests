package com.amboss;

import com.amboss.base.TestUtilities;
import com.amboss.pages.Category;
import com.amboss.pages.FilterModal;
import com.amboss.pages.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

public class SessionSetupTests extends TestUtilities {

    @Test
    public void unfoldCategoryAndSelectSubcategory() {
        SoftAssert softAssert = new SoftAssert();
        LoginPage loginPage = new LoginPage(driver);

        FilterModal modal = loginPage.login()
                .openCustomSession()
                .openFilterModal("Symptoms");

        Assert.assertTrue(modal.isOpened());

        Category symptoms = modal.getTitleCategory();
        softAssert.assertTrue(symptoms.isNeutralChecked());

        Category headAndNeck = modal.getCategory("Head and neck")
                .unfold();
        softAssert.assertTrue(headAndNeck.isNeutralChecked());

        Category eye = modal.getCategory("Eye")
                .unfold();
        softAssert.assertTrue(eye.isNeutralChecked());

        Category eyelidDisorders = modal.getCategory("Eyelid disorders");
        softAssert.assertTrue(eyelidDisorders.isNeutralChecked());

        eyelidDisorders.select();

        softAssert.assertTrue(symptoms.isMinus());
        softAssert.assertTrue(headAndNeck.isMinus());
        softAssert.assertTrue(eye.isMinus());
        softAssert.assertTrue(eyelidDisorders.isChecked());

        softAssert.assertAll();
    }
}
